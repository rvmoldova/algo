#include <fstream>
#include <string>
#include <vector>

std::ifstream fin("strmatch.in");
std::ofstream fout("strmatch.out");

std::pair<std::vector<size_t>, unsigned int> KMP(std::string needle, std::string hatch) {
	std::size_t pos = hatch.find(needle);
	std::vector<std::size_t> results;
	unsigned int count = 0;
	while (1) {
		if (pos == std::string::npos) {
			break;
		}
		else {
			count++;
			if (results.size() < 1000) {
				results.push_back(pos);
			}
		}
		pos = hatch.find(needle, pos + 1);
	}
	return std::make_pair(results, count);
}

int main() {
	std::string a, b;
	fin >> a >> b;
	std::pair<std::vector<size_t>, unsigned int> res = KMP(a, b);
	fout << res.second << "\n";
	for (std::vector<size_t>::iterator it = res.first.begin(); it != res.first.end(); it++) {
		fout << *it << " ";
	}
	return 0;
}